# -*- mode: python; tab-width: 4; indent-tabs-mode: nil -*-
#
# $Id: SQZ_LO.py $
# $HeadURL: https://redoubt.ligo-wa.caltech.edu/svn/cds_user_apps/trunk/sqz/h1/guardian/SQZ_BEATNOTE.py $

import sys
import time
from guardian import GuardState, GuardStateDecorator

nominal = 'DOWN'
class INIT(GuardState):
    index = 0

    def main(self):
        return True

class DOWN(GuardState):
    index = 1
    goto = True

    def main(self):
        return True

    def run(self):
        return True

class IDLE(GuardState):
    index = 3
    request = False

    def run(self):
        return True


class LOCK_BEATNOTE(GuardState):
    index = 10
    goto  = True

    def main(self):
        self.timer['slow_cycle'] = 1
        return
    
    def run(self):
        if self.timer['slow_cycle']:
            # if the timer is done, reset and adjust temperature
            self.timer['slow_cycle'] = 1

            BN_DELTA_MAX = 50e6      # maximum difference beat note for servo action, in Hz
            BN_PZTVOLT_MAX = 100     # maximum PZTVOLT for servo
            BN_PZTVOLT_MIN = 0     # minimum PZTVOLT for servo
            DELTA_PZT_MAX = 0.1     # largest step servo will take


            ref = 0
            delta_BN = ref-ezca['SQZ-FIBR_LOCK_BEAT_FREQUENCYERROR'] 

            if ezca['SQZ-OPO_TRANS_DC_NORMALIZED']<0.5:
               notify('OPO not locked')
               log('OPO not locked')

            elif abs(delta_BN) > BN_DELTA_MAX:
              print("Beat note frequency in LA-LA-LAND")

            else:   
              delta_pzt_volt = delta_BN*7e-8*0.05 #in volt
              if delta_pzt_volt > DELTA_PZT_MAX:
                delta_pzt_volt = DELTA_PZT_MAX
              elif delta_pzt_volt < -DELTA_PZT_MAX:
                delta_pzt_volt = -DELTA_PZT_MAX

              new_pzt_volt = ezca['SQZ-OPO_PZT_1_OFFSET'] + delta_pzt_volt
              if new_pzt_volt < BN_PZTVOLT_MIN:
                ezca['SQZ-OPO_PZT_1_OFFSET'] = BN_PZTVOLT_MIN
                notify('PZT at minimum')
                log('PZT at minimum')  
              elif new_pzt_volt > BN_PZTVOLT_MAX:
                ezca['SQZ-OPO_PZT_1_OFFSET'] = BN_PZTVOLT_MAX
                notify('PZT at minimum')
                log('PZT at minimum') 
              else:
                ezca['SQZ-OPO_PZT_1_OFFSET'] = new_pzt_volt

        return True

##################################################

edges = [
    ('INIT', 'IDLE'),
    ('INIT', 'DOWN'),
    ('IDLE', 'DOWN'),
    ('DOWN','LOCK_BEATNOTE')
]


